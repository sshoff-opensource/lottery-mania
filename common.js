import validator from 'validator';
import { auth } from './auth.js';

export function successResponse(res, msg) {
    const response = {
        status: 'ok',
        result: msg,
    };

    res.status(200).json(response);
}

export function errorResponse(res, status, msg) {
    const err = new Error();

    err.status = 'error';
    err.message = msg;

    res.status(status).json(err);
}

export async function validateToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];

    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];

        if (validator.isJWT(bearerToken)) {
            req.token = bearerToken;

            try {
                const authResult = await auth(bearerToken);

                if (!authResult) {
                    errorResponse(res, 403, 'Forbidden');
                } else {
                    req.userId = authResult;
                    next();
                }
            } catch (err) {
                errorResponse(res, 400, err.message);
            }
        } else {
            errorResponse(res, 400, 'Bad JWT Token');
        }
    } else {
        errorResponse(res, 403, 'Forbidden');
    }
}