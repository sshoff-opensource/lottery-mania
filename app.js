import dotenv from 'dotenv';
import express from 'express';
import * as uuid from 'uuid';
import Db from './db.js';
import { jwtGetToken } from './auth.js';
import Board from './board.js';
import { successResponse, errorResponse, validateToken } from './common.js'
import { authenticationInputValidation, checkBoardInputValidation } from './input-validation.js'

dotenv.config();
const port = parseInt(process.env.PORT, 10) || 3000;

const db = Db.getInstance();

const app = express();


app.get('/authentication', async (req, res) => {
    const clienId = req.query.clientId;

    const validationError = authenticationInputValidation(clienId);

    if (validationError !== true) {
        errorResponse(res, 400, validationError);
        return;
    }

    const token = jwtGetToken(clienId);

    await db.saveOne('user', clienId, token);

    res.header('Authentication', token);

    res.status(200).send();
});


app.post('/create-board', validateToken, async (req, res) => {
    const board = new Board();
    board.readConfig();

    const game = board.getBoard();
    const boardId = uuid.v4();

    const gameData = {
        userId: req.userId,
        board: game
    };

    await db.saveOne('game', boardId, gameData);

    successResponse(res, { boardId });
});


app.get('/check-board', validateToken, async (req, res) => {
    const boardId = req.query.boardId;
    const firstAnswer = parseInt(req.query.firstAnwser, 10);
    const secondAswer = parseInt(req.query.secondAnwser, 10);

    const validationError = checkBoardInputValidation(boardId, firstAnswer, secondAswer);

    if (validationError !== true) {
        errorResponse(res, 400, validationError);
        return;
    }

    const game = await db.readOne('game', boardId);

    if (!game) {
        errorResponse(res, 400, `Game ${boardId} not found.`);
        return;
    }

    if (req.userId !== game.userId) {
        errorResponse(res, 400, `The game ${boardId} is not owned by you.`);
        return;
    }

    const board = new Board();
    let result = {
        board: game.board,
        result: null
    };

    // TODO: Mark that the game has been played and check it
    if (board.checkAnswers(game.board, firstAnswer, secondAswer)) {
        result.result = 'win';
    } else {
        result.result = 'loss';
    }

    successResponse(res, result);
});


if (db.isReady()) {
    app.listen(port, () => {
        console.log(`Service started successfully on port ${port}.`);
    });
} else {
    throw new Error('Database is not ready.');
}
