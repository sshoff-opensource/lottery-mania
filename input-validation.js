import validator from 'validator';

export function authenticationInputValidation(clienId) {
    if (!validator.isUUID(clienId)) {
        return 'Field `cliendId` must be valid UUID.';
    }

    return true;
}

export function checkBoardInputValidation(boardId, firstAnswer, secondAswer) {
    if (!boardId) {
        return 'Field `boardId` is obligatory.';
    }

    if (!validator.isUUID(boardId)) {
        return 'Field `boardId` must be valid UUID.';
    }

    if (!firstAnswer) {
        return 'Field `firstAnwser` is obligatory and must be integer.';
    }

    if (!secondAswer) {
        return 'Field `secondAnwser` is obligatory and must be integer.';
    }

    if (firstAnswer < 1 || firstAnswer > 99) {
        return 'Field `firstAnwser` must be from 1 to 99.';
    }

    if (secondAswer < 1 || secondAswer > 99) {
        return 'Field `secondAnwser` must be from 1 to 99.';
    }

    return true;
}
