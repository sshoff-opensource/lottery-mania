import { MongoClient, ServerApiVersion } from 'mongodb';
import dotenv from 'dotenv';

dotenv.config();

class MongoDbWorker {
    constructor() {
    }

    async init(){
        const mongoDbHost = process.env.MONGODB_HOST || '127.0.0.1';
        const mongoDbPort = parseInt(process.env.MONGODB_PORT, 10) || 27017;
        const mongoDbUser = process.env.MONGODB_USER || 'root';
        const mongoDbPassword = process.env.MONGODB_PASSWORD;
        const mongoDbDatabase = process.env.MONGODB_DATABASE || 'lottery';
        
        const mongoDbDsn = `mongodb://${mongoDbUser}:${mongoDbPassword}@${mongoDbHost}:${mongoDbPort}/`;
        
        const mongoDbClient = new MongoClient(mongoDbDsn, {
            serverApi: {
                version: ServerApiVersion.v1,
                strict: true,
                deprecationErrors: true,
            }
        });

        this.db = mongoDbClient.db(mongoDbDatabase);
        
        try {
            await mongoDbClient.connect();
        } catch (error) {
            throw new Error(error);
        }
    }

    isReady() {
        return true;
    }

    async saveOne(entity, id, value) {
        const collection = this.db.collection(entity);

        const document = {
            id,
            data: value
        };

        return await collection.insertOne(document);
    }

    async readOne(entity, id) {
        const collection = this.db.collection(entity);

        const data = await collection.findOne({ id });

        if (data) {
            return data.data;
        } else {
            return undefined;
        }
    }
}

const mongoDbWorker = new MongoDbWorker();

await mongoDbWorker.init();

export default mongoDbWorker;