import redisDbWorker from './redis.js';
import mongoDbWorker from './mongo.js';
import dotenv from 'dotenv';

dotenv.config();

const dbType = process.env.DB_TYPE || 'redis';

export default class Db {
    static _instance;

    constructor() {
    }

    static getInstance() {
        if (!Db._instance) {
            Db._instance = new this();

            if (dbType === 'redis') {
                Db._instance.db = redisDbWorker;
            } else if (dbType === 'mongodb') {
                Db._instance.db = mongoDbWorker;
            }
        }

        return Db._instance;
    }

    isReady(){
        return this.db.isReady();
    }

    async saveOne(entity, id, value) {
        return await this.db.saveOne(entity, id, value)
    }

    async readOne(entity, id) {
        return await this.db.readOne(entity, id);
    }
}