import { createClient } from 'redis';
import dotenv from 'dotenv';

dotenv.config();

class RedisDb {
    constructor() {
    }

    async init(){
        const redisHost = process.env.REDIS_HOST || '127.0.0.1';
        const redisPort = parseInt(process.env.REDIS_PORT, 10) || 6379;
        const redisUser = process.env.REDIS_USER || 'default';
        const redisPassword = process.env.REDIS_PASSWORD;
        
        const redisClient = createClient({
            url: `redis://${redisUser}:${redisPassword}@${redisHost}:${redisPort}`
        });

        this.db = redisClient;
        
        try {
            await redisClient.connect();
        } catch (error) {
            throw new Error(error);
        }
    }

    isReady() {
        return this.db.isReady;
    }

    async saveOne(entity, id, value) {
        return await this.db.set(`${entity}:${id}`, JSON.stringify(value));
    }

    async readOne(entity, id) {
        const data = await this.db.get(`${entity}:${id}`);

        if (data) {
            return JSON.parse(data);
        } else {
            return undefined;
        }
    }
}

const redisDbWorker = new RedisDb();

await redisDbWorker.init();

export default redisDbWorker;
