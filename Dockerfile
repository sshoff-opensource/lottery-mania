FROM node:20-alpine3.17 as dev

LABEL org.opencontainers.image.authors="Sergey SH <sshoff@sshoff.com>"

WORKDIR /app

COPY --chown=node:node package*.json ./

RUN npm ci

COPY --chown=node:node . .

USER node

ENV NODE_ENV development



FROM node:20-alpine3.17 as prod

WORKDIR /app

COPY --chown=node:node --from=dev /app/node_modules ./node_modules

COPY --chown=node:node . .

ENV NODE_ENV production

RUN npm ci --only=production && npm cache clean --force

USER node

CMD node app.js
