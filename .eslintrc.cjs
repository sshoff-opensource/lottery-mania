module.exports = {
    env: {
        es6: true,
        node: true,
    },
    extends: [
        "airbnb-base",
        "eslint:recommended",
        "plugin:prettier/recommended"
    ],
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly",
    },
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: "module",
    },
    // plugins: [],
    root: true,
    env: {
        node: true,
        jest: true,
    },
    ignorePatterns: ['.eslintrc.js'],
    rules: {
        "no-console": "off", // "warn" // "off"
    },
    settings: {},
};