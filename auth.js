import dotenv from 'dotenv';
import Db from './db.js';
import jwt from 'jsonwebtoken';

dotenv.config();

const jwtSecretKey = process.env.JWT_SECRET_KEY;

export function jwtGetToken(uuid) {
    const payload = {
        cliendId: uuid,
    };

    return jwt.sign(payload, jwtSecretKey);
}

export async function auth(token) {
    const payload = jwt.verify(token, jwtSecretKey, (err, pl) => {
        if (err) {
            throw new Error(err.message);
        } else {
            return pl;
        }
    });

    const db = Db.getInstance();

    const result = await db.readOne('user', payload.cliendId);

    if (result) {
        return payload.cliendId;
    } else {
        return false;
    }
}
