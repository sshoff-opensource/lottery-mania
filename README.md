# ENV file

`.env`:
```
NODE_ENV=development
PORT=3000

DB_TYPE=redis
# DB_TYPE=mongodb

REDIS_HOST="redis"
REDIS_PORT=6379
REDIS_USER="default"
REDIS_PASSWORD="zaq12wsx"

MONGODB_HOST="mongo"
MONGODB_PORT=27017
MONGODB_USER="root"
MONGODB_PASSWORD="zaq12wsx"
MONGODB_DATABASE="lottery"

JWT_SECRET_KEY="bXF!LCMXLZhksnR<N2S9t9xF"

```

# How to run

```
cd docker && docker-compose up --build -d
```

# How to play

1. Get JWT-token:
```
curl --location '127.0.0.1:3000/authentication?clientId=<UUIDv4>'
```

Get `JWT Token` from the header `Authentication`.

2. Create a game:
```
curl --location --request POST '127.0.0.1:3000/create-board' \
--header 'Authorization: Bearer <JWT Token>'
```

Get the UUID of the game from the response field `result.boardId`.

3. Play:
```
curl --location '127.0.0.1:3000/check-board?boardId=<boardId>firstAnwser=<number>&secondAnwser=<number>' \
--header 'Authorization: Bearer <JWT Token>'
```