import fs from 'fs';

export default class Board {
    config

    constructor() {
    }

    readConfig() {
        const rawData = fs.readFileSync('numbers-and-groups.json');
        this.config = JSON.parse(rawData);
        this.validateConfig();
    }

    getConfig() {
        return this.config;
    }

    validateConfig() {
        let sumWeight = 0;
        let numbersCount = 0;
        let allNumbers = [];

        if (!Array.isArray(this.config)) {
            throw new Error('Config must be array of objects.');
        }

        this.config.groups = {};

        const configLen = this.config.length;

        for (let ii = 0; ii < configLen; ii++) {
            const element = this.config[ii];

            if (element.weight === undefined) {
                throw new Error(`Object ${ii} doesn't contain obligatory field 'weight'.`);
            }
            if (element.numbers === undefined) {
                throw new Error(`Object ${ii} doesn't contain obligatory field 'numbers'.`);
            }

            sumWeight = sumWeight + element.weight;
            this.config.groups[ii] = element.weight;

            if (!Array.isArray(element.numbers)) {
                throw new Error(`The field 'numbers' must be array of objects.`);
            }

            const numbersLen = element.numbers.length;
            element.values = [];
            for (let jj = 0; jj < numbersLen; jj++) {
                if (element.numbers[jj].start === undefined) {
                    throw new Error(`Fhe field 'numbers' ${jj} in the object ${ii} doesn't contain obligatory field 'start'.`);
                }
                if (element.numbers[jj].end === undefined) {
                    throw new Error(`Fhe field 'numbers' ${jj} in the object ${ii} doesn't contain obligatory field 'end'.`);
                }

                for (let kk = element.numbers[jj].start; kk <= element.numbers[jj].end; kk++) {
                    element.values.push(kk);
                }
            }
            numbersCount = numbersCount + element.values.length;
            allNumbers = allNumbers.concat(element.values);
        }

        allNumbers.sort((a, b) => a - b);

        if (sumWeight !== 1) {
            throw new Error('The sum of the weights is not equal to 1.');
        }

        if (numbersCount !== 99) {
            throw new Error('The number of numbers is less than 99.');
        }

        for (let ll = 1; ll <= 99; ll++) {
            if (allNumbers[ll - 1] != ll) {
                throw new Error(`There is an error in the numbers sequence. Something wrong with ${ll}.`);
            }
        }
    }

    getBoard(len = 10) {
        let groups = this.config.groups;
        let boardNumbers = [];

        for (let ii = 1; ii <= len; ii++) {
            let group = this.randomGroupWeighted(groups);

            let groupLen = this.config[group].values.length;

            let added = false;
            while (added != true) {
                let number = this.config[group].values[Math.floor(Math.random() * groupLen)];
                if (boardNumbers.includes(number)) {
                    continue;
                } else {
                    boardNumbers.push(number);
                    added = true;
                }
            }
        }

        return boardNumbers;
    }

    checkAnswers(board, firstAssumption, secondAssumption) {
        if (board.includes(firstAssumption) && board.includes(secondAssumption)) {
            return true;
        }

        return false;
    }

    randomGroupWeighted(groups) {
        let table = [];

        for (let ii in groups) {
            for (let jj = 0; jj < groups[ii] * 100; jj++) {
                table.push(ii);
            }
        }

        return table[Math.floor(Math.random() * table.length)];
    }
}